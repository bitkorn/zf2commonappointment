<?php

namespace CommonAppointment;

return array(
    'router' => array(
        'routes' => array(
//            'event_arrangement_index_month' => array(
//                'type' => 'Segment',
//                'options' => array(
//                    'route' => '/event-arrangement-month[/]',
//                    'defaults' => array(
//                        '__NAMESPACE__' => 'EventArrangement\Controller',
//                        'controller' => 'EventFront',
//                        'action' => 'indexMonth',
//                    ),
//                ),
//            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
        ),
        'invokables' => array(
        ),
    ),
    'translator' => array(
//        'locale' => 'de_DE', // BitkornListener->setupLocalization() kann das hier nicht überschreiben
        'translation_file_patterns' => array(
            /*
             * Das hier kann auch in der Module.php->onBootstrap()
             * $this->translator->addTranslationFilePattern($type, $baseDir, $pattern)
             * 
             * zum kategorisieren kann man z.B.:
             * 'pattern'  => 'bitkorn_%s.mo'
             * ...%s ist der language code
             */
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
                'text_domain' => 'common_appointment'
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
//            'template/eventMonth' => __DIR__ . '/../view/template/eventMonth.phtml',
        ),
        'template_path_stack' => array(
            'CommonAppointment' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'common_appointment' => [
        
    ],
);
