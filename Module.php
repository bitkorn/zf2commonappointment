<?php

namespace CommonAppointment;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

/**
 * 
 */
class Module implements AutoloaderProviderInterface, ServiceProviderInterface, ControllerProviderInterface, ViewHelperProviderInterface
{

    public function onBootstrap(MvcEvent $e)
    {
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
//                'BitkornCalendar\Controller\Test' => function(\Zend\Mvc\Controller\ControllerManager $serviceLocator) {
//                    $ctr = new \BitkornCalendar\Controller\EventFrontController();
//                    return $ctr;
//                },
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                /*
                 * Table
                 */
//                'BitkornCalendar\Table\Event\Event' => function(\Zend\ServiceManager\ServiceManager $serviceManager) {
//                    $table = new Table\Event\EventTable();
//                    $table->setDbAdapter($serviceManager->get('dbGlobal'));
//                    $table->setLogger($serviceManager->get('logger'));
//                    return $table;
//                },
            ]
        ];
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => [
//                'eventMonth' => function (\Zend\View\HelperPluginManager $hpm) {
//                    $sl = $hpm->getServiceLocator();
//                    $helper = new View\Helper\Event\EventMonth();
//                    $helper->setEventTable($sl->get('EventArrangement\Table\Event\Event'));
//                    return $helper;
//                },
            ]
        );
    }

}
